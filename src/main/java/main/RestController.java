package main;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.geojson.GeoJsonObject;
import org.postgis.Geometry;
import org.postgis.Point;
import org.postgis.geojson.PostGISModule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;


@Controller
public class RestController implements WebMvcConfigurer {

    @Autowired
    private FarmRepository farmRepository;

    @Autowired
    private FieldRepository fieldRepository;

    @GetMapping("farms")
    public ResponseEntity<List<Farm>> getAllFarms() {
        List<Farm> list = new ArrayList<>();
        farmRepository.findAll().forEach( farm -> {
            list.add(farm);
        });
        return new ResponseEntity<List<Farm>>(list, HttpStatus.OK);
    }

    @GetMapping("farms/{id}")
    public ResponseEntity<Farm> getFarm(@PathVariable Long id) {
        return new ResponseEntity<Farm>(farmRepository.findById(id).get(), HttpStatus.OK);
    }

    @PostMapping("farms")
    ResponseEntity newFarm(@RequestBody FarmDto farmDto) {

        ResponseEntity response = null;

        Farm farm = new Farm();
        List<Field> fields = new ArrayList<>();

        try {
            farmDto.getFieldsId().forEach(fieldId ->
            {
                fields.add(fieldRepository.findById(fieldId).get());

                farm.setName(farmDto.getName());
                farm.setNote(farmDto.getNote());

                farm.setFields(fields);

                Farm newFarm = farmRepository.save(farm);
            });
            response = ResponseEntity.ok().build();
        } catch (NoSuchElementException e) {
            System.err.println(e.getMessage());
            response = ResponseEntity.badRequest().build();
        }

        return response;
    }

    @DeleteMapping("farms/{id}/delete")
    public ResponseEntity<Farm> deleteFarm(@PathVariable Long id) {

        farmRepository.delete(farmRepository.findById(id).get());

        return new ResponseEntity<Farm>(HttpStatus.OK);

    }

    @PutMapping("farms/{id}/update")
    public ResponseEntity<Farm> updateFarm(@PathVariable Long id) {

        Farm farm = farmRepository.findById(id).get();

        Farm newFarm = farmRepository.save(farm);

        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("{id}").buildAndExpand(newFarm.getId()).toUri();
        return ResponseEntity.created(location).build();
    }

    @GetMapping("fields")
    public ResponseEntity<List<Field>> getAllFields() {
        List<Field> list = new ArrayList<>();
        fieldRepository.findAll().forEach( field -> {
            list.add(field);
        });
        return new ResponseEntity<List<Field>>(list, HttpStatus.OK);
    }

    @GetMapping("fields/{id}")
    public ResponseEntity<Field> getField(@PathVariable Long id) {
        return new ResponseEntity<Field>(fieldRepository.findById(id).get(), HttpStatus.OK);
    }

    @PostMapping("fields")
    ResponseEntity newField(@RequestBody FieldDto fieldDto) {

        ResponseEntity response = null;

        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new PostGISModule());

        try{
            Farm farm = farmRepository.findById(fieldDto.getFarmId()).get();

            Field field = new Field();



            String geo = mapper.writeValueAsString(fieldDto.getGeometry());

            Geometry geometry = (Geometry)  mapper.readValue(geo, Geometry.class);

            if(geometry.getType() == 3){
                field.setFarm(farm);
                field.setName(fieldDto.getName());
                field.setGeometry(geometry);

                Field newFiled = fieldRepository.save(field);

                response = ResponseEntity.ok().build();
            }else {
                response = ResponseEntity.badRequest().build();
            }



        } catch (NoSuchElementException e){
            System.err.println(e.getMessage());
            response = ResponseEntity.badRequest().build();
        } catch (JsonMappingException e) {
            e.printStackTrace();
            response = ResponseEntity.badRequest().build();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            response = ResponseEntity.badRequest().build();
        }

        return response;
    }

    @DeleteMapping("fields/{id}/delete")
    public ResponseEntity<Field> deleteField(@PathVariable Long id) {

       fieldRepository.delete(fieldRepository.findById(id).get());

       return new ResponseEntity<Field>(HttpStatus.OK);

    }

    @PutMapping("fields/{id}/update")
    public ResponseEntity<Field> updateField(@PathVariable Long id) {

        Field field = fieldRepository.findById(id).get();

        Field newFiled = fieldRepository.save(field);

        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("{id}").buildAndExpand(newFiled.getId()).toUri();
        return ResponseEntity.created(location).build();
    }

}
