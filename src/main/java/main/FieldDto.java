package main;


import com.fasterxml.jackson.annotation.JsonSubTypes;
import org.geojson.Geometry;
import org.geojson.Polygon;

public class FieldDto {
    private String name;
    private Long farmId;

    @JsonSubTypes.Type(Polygon.class)
    private Geometry geometry;

    public Geometry getGeometry() {
        return geometry;
    }

    public void setGeometry(Geometry geometry) {
        this.geometry = geometry;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getFarmId() {
        return farmId;
    }

    public void setFarmId(Long farmId) {
        this.farmId = farmId;
    }
}
