package main;

import org.postgis.Geometry;

import javax.persistence.*;

@Entity
@Table(name = "Fields")
public class Field {
    private @Id
    @GeneratedValue
    Long id;

    @ManyToOne
    @JoinColumn(name = "farm_id")
    private Farm farm;

    private String name;

    @Column(columnDefinition = "geometry(Polygon)")
    private Geometry geometry;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGeometry() {
        return geometry.toString();
    }

    public void setGeometry(Geometry geometry) {
        this.geometry = geometry;
    }

    public Farm getFarm() {
        return farm;
    }

    public void setFarm(Farm farm) {
        this.farm = farm;
    }

}
