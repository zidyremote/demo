package main;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "Farms")
public class Farm {
    private @Id
    @GeneratedValue
    Long id;

    @OneToMany(mappedBy = "id")
    private List<Field> fields;

    private String name;

    private String note;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Field> getFields() {
        return fields;
    }

    public void setFields(List<Field> fields) {
        this.fields = fields;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
