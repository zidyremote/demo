package main;

import java.util.List;

public class FarmDto {

    private List<Long> fieldsId;

    private String name;

    private String note;

    public List<Long> getFieldsId() {
        return fieldsId;
    }

    public void setFieldsId(List<Long> fieldsId) {
        this.fieldsId = fieldsId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
